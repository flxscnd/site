
f = io.popen('ls /var/www/localhost/store')

q = ''
while true do
  l = f:read('*l')

  if l == nil then break end
  q = q .. '\
  <div class="entry">\
    <div class="text">' .. l .. '</div>\
      <a href="' .. l .. '" class="btn btn-download">Download</a>\
    </div>\
  '
  end

f:close()

-- s = '\
--   <html>\
--     <body>\
--       <a href="/backup">create backup</a><hr>backups:<br>' .. q .. '\
--     </body>\
-- </html>'

s = '\
<!DOCTYPE html>\
<html lang="en">\
  <head>\
    <meta charset="UTF-8" />\
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />\
    <title>List of backups</title>\
    <link rel="stylesheet" href="style.css" />\
  </head>\
  <body>\
    <h1>Backups list</h1>\
    <div class="container">\
      <a class="btn btn-create">Create manual backup</a>\
      '.. q .. '\
      </div>\
    </div>\
  </body>\
</html>\
'

lighty.header['Content-Type'] = 'text/html'
lighty.content = {s}
return 200
